package com.example.openshiftdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@RestController
class OpenshiftDemoApplication {
    @GetMapping
    fun sayHello() = "hello from openshift!"
}

fun main(args: Array<String>) {
    runApplication<OpenshiftDemoApplication>(*args)
}
